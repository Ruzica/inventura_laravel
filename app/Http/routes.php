<?php
use App\Employee;

use App\Location;

use App\Equipment;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

Route::get('/', 'HomeController@index');
Route::resource('/employees', 'EmployeesController');
Route::resource('/equipment', 'EquipmentsController');
Route::resource('/equipmentOwner', 'EquipmentOwnersController');
Route::resource('/locations', 'LocationsController');


Route::get('/locations', 'LocationsController@index')->middleware('auth');
//Route::get('/locations/create', 'LocationsController@create')->middleware('auth');
//Route::get('locations/{locations} ', 'LocationsController@update')->middleware('auth');
//Route::get('locations/{locations}/edit ', 'LocationsController@edit')->middleware('auth');
//Route::get('locations/{locations} ', 'LocationsController@destroy')->middleware('auth');

Route::get('/employees', 'EmployeesController@index')->middleware('auth');
//Route::get('/employees/create', 'EmployeesController@create')->middleware('auth');
//Route::get('/employees/{employees}', 'EmployeesController@update')->middleware('auth');
//Route::get('/employees/{employees}', 'EmployeesController@update')->middleware('auth');
//Route::get('/employees/{employees}', 'EmployeesController@destroy')->middleware('auth');
//Route::get('/employees/{employees}/edit', 'EmployeesController@edit')->middleware('auth');


Route::get('/equipment', 'EquipmentsController@index')->middleware('auth');


Route::get('/equipmentOwner', 'EquipmentOwnersController@index')->middleware('auth');

//Route::get('/equipmentOwner/create', 'EquipmentOwnersController@edit')->middleware('IsAdmin');

Route::resource('/users', 'UsersController');