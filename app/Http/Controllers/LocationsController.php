<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Location as Location;

class LocationsController extends Controller
{

    /*public function __construct() {
        $this->middleware('IsAdmin', ['except'=>['index_location']
        ]);
    }*/




    public function index()
    {
       $users = User::all();
       $locations = Location::all();
        return view('locations.index', compact('locations', 'users'));
    }


    public function create()
    {
        return view('locations.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:50',
            'number_of_employees'=>'required'
        ]);


        Location::create($request->all()); //sprema lokaciju u bazu

        return redirect('/locations');

    }


    public function show($id) //nije potrebno za sad
    {
        $location = Location::findOrFail($id);

        return view('locations.show', compact('location'));
    }


    public function edit($id)
    {
        $location = Location::findOrFail($id);

        return view('locations.edit', compact('location'));
    }


    public function update(Request $request, $id)
    {
        $location = Location::findOrFail($id);

        $location->update($request->all());
        return redirect('locations');
    }


    public function destroy($id)
    {
        $location = Location::findOrFail($id);

        $location->delete();

        return redirect('locations');
    }

}
