<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelQRCode\Facades\QRCode;
use App\Http\Requests;
use App\Equipment;
use App\Location;
use App\EquipmentOwner;

class EquipmentsController extends Controller
{

    public function index()
    {
        $equipments = Equipment::all();
        return view('equipment.index', compact('equipments'));
    }


    public function create()
    {
        $locations = Location::all(['id', 'name']);
        $equipmentOwners = EquipmentOwner::all(['id', 'name']);

        return view('equipment.create', compact('locations', 'equipmentOwners'));
    }


    public function store(Request $request)
    {

        $input = $request->all();

        $this->validate($request, [
            'name'=>'required|max:50',
            'pn_number' =>'required|max:20',
            'serial_number' =>'required|max:20',
            'model' =>'required|max:50',
            'comment' =>'required|max:500',
        ]);

       $equipment = Equipment::create($input);


        $file = 'qrcodes/'.$equipment->id.'.png';


        QRCode::text("Naziv opreme:  $equipment->name | PN broj: $equipment->pn_number | Serijski broj: $equipment->serial_number | Model opreme: $equipment->model")
            ->setSize(4)
            ->setMargin(2)
            ->setOutfile($file)
            ->png();

        $input['qrcode_path'] = $file;


         Equipment::where('id', $equipment->id)->update(['qrcode_path' =>  $input['qrcode_path']]);


        return redirect('/equipment');
    }


    public function show($id)
    {
        $equipment = Equipment::findOrFail($id);
        $locations = Location::all();
        $equipmentOwners = EquipmentOwner::all();

        return view('equipment.show', compact('equipment', 'locations', 'equipmentOwners'));
    }


    public function edit($id)
    {
        $equipment = Equipment::findOrFail($id);
        $locations = Location::all();
        $equipmentOwners = EquipmentOwner::all();

        return view('equipment.edit', compact('equipment', 'locations', 'equipmentOwners'));
    }


    public function update(Request $request, $id)
    {
        $equipment = Equipment::findOrFail($id);

        $equipment->update($request->all());
        return redirect('equipment');
    }


    public function destroy($id)
    {
        $equipment = Equipment::findOrFail($id);

        $equipment->delete();

        return redirect('equipment');
    }
}
