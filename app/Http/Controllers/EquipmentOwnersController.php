<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\EquipmentOwner;
use Illuminate\Http\Request;
use App\Http\Requests;

class EquipmentOwnersController extends Controller
{

    public function index()
    {
        $equipmentOwners = EquipmentOwner::all();
        return view('equipmentOwner.index', compact('equipmentOwners'));
    }


    public function create()
    {
        return view('equipmentOwner.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:50',
            'company'=>'required|max:50',
            'comment'=>'required|max:500'
        ]);

        EquipmentOwner::create($request->all()); //sprema lokaciju u bazu

        return redirect('/equipmentOwner');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $equipmentOwner = EquipmentOwner::findOrFail($id);

        return view('equipmentOwner.edit', compact('equipmentOwner'));
    }

    public function update(Request $request, $id)
    {
        $equipmentOwner = EquipmentOwner::findOrFail($id);

        $equipmentOwner->update($request->all());
        return redirect('equipmentOwner');
    }

    public function destroy($id)
    {
        $equipmentOwner = EquipmentOwner::findOrFail($id);

        $equipmentOwner->delete();

        return redirect('equipmentOwner');
    }
}
