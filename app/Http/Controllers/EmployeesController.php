<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employee;
use App\Location;

class EmployeesController extends Controller
{

    public function index()
    {
        $employees = Employee::all();
        return view('employees.index', compact('employees'));
    }

    public function create()
    {
        $locations = Location::all(['id', 'name']);

        return view('employees.create', compact('locations'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:50',
        ]);

        $employee = new Employee;
        $employee->name = $request->name;
        $employee->location_id = $request->location_id;
        $employee->save();

        return redirect('/employees');


    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        $locations = Location::all(['id', 'name']);

        return view('employees.edit', compact('employee', 'locations'));
    }


    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);

        $employee->update($request->all());
        return redirect('employees');
    }

    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);

        $employee->delete();

        return redirect('employees');
    }
}

