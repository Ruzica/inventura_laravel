<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentOwner extends Model
{
    protected $fillable= ['name', 'company', 'comment'];
}
