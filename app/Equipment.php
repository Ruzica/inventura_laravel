<?php

namespace App;
use App\Location;
use App\EquipmentOwner;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $fillable = [

        'name',
        'serial_number',
        'pn_number',
        'model',
        'comment',
        'location_id',
        'equipment_owner_id'

    ];

    public function location(){

        return $this->belongsTo('App\Location');

    }

    public function equipmentOwner(){

        return $this->belongsTo('App\EquipmentOwner');

    }
}
