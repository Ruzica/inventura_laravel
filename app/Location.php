<?php

namespace App;
use App\Employee;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable= ['name', 'number_of_employees'];

    public function employees(){

        return $this->hasMany('App\Employee', 'employee_id');

    }


}
