# Inventory Appllication
> Web application for inventory and equipment organization.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Screenshots](#screenshots)
* [Project Status](#project-status)


## General Information
- The application is based on user login and registration. 
- The data related to the company organization is linked to the database.
- Application can be expanded with new database parameters related to the company.
- There is possibility of QR scanning, data export in several formats, data copying and printing.



## Technologies Used
- PHP - version 5.5.9
- HTML - version 5.2
- CSS 
- Bootstrap - version 3.3.6
- MySQL 
- Laravel - version 5.2.45


## Screenshots
![Example screenshot](./public/dist/img/bild1.png)
![Example screenshot](./public/dist/img/bild2.png)
![Example screenshot](./public/dist/img/bild3.png)
![Example screenshot](./public/dist/img/bild4.png)


## Project Status
Project is: _complete_ 


