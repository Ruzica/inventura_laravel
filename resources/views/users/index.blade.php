@extends ('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="box-body">

            <div class="panel panel-default">
                <div class="panel-heading">Korisnici</div>
                <div class="panel-body">
                <table class="table table-bordered table-hover dtable">
        <thead align="center">
        <tr>
            <th>Ime korisnika</th>
            <th>Pravo pristupa korisnika</th>
            @if(Auth::user()->role == "Administrator")

            @endif
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)

            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->role}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

            </div>
        </div>
    </div>    </div>

@stop

@extends('layouts.footer')