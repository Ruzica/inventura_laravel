@extends ('layouts.admin')

@section('content')


    <h1>UREDI ZAPOSLENIKA</h1></div>

    {!! Form::model($user,['method'=>'PATCH', 'action'=>['UsersController@update', $user->id]]) !!}
    {{csrf_field()}}

    {!! Form::label('name', 'Ime korisnika') !!}

    {!! Form::text('name', null, ['class'=>'form-control']) !!}


    <div class="form-group">
        {!! Form::Label('name', 'Pravo pristupa') !!}
        <select class="form-control" name="location_id">
            @foreach($users as $user)
                <option value="{{$user->id}}"
                @if ($user->role == $user->role )
                    {{ 'selected' }}
                        @endif
                >{{$user->role}}</option>
            @endforeach
        </select>
    </div>



    {!! Form::submit('Uredi korisnika', ['class'=>'btn btn-primary']) !!}

    {!! Form::close() !!}




@stop

@extends('layouts.footer')