@extends ('layouts.admin')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading">Vlasnik opreme</div>
                <div class="panel-body">
                <table class="table table-bordered table-hover dtable">
        <thead align="center">
        <tr>
            <th>Ime vlasnika opreme</th>
            <th>Naziv tvrtke</th>
            <th>Komentar</th>
            @if(Auth::user()->role == "Administrator")
                <th></th>
                <th></th>
            @endif
        </tr>
        </thead>
        <tbody>

        @foreach($equipmentOwners as $equipmentOwner)
            <tr>
                <td>{{$equipmentOwner->name}}</td>
                <td>{{$equipmentOwner->company}}</td>
                <td>{{$equipmentOwner->comment}}</td>
                @if(Auth::user()->role == "Administrator")
                    <td><div class="text-center" ><a href="{{route('equipmentOwner.edit', $equipmentOwner->id)}}"><i class="glyphicon glyphicon-edit"></i></a></div></td>
                    <td><div class="text-center" ><a href="{{route('equipmentOwner.destroy', $equipmentOwner->id)}}"><i class="glyphicon glyphicon-trash"></i></a></div></td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
                    @if(Auth::user()->role == "Administrator")
                        <button type="submit" class="btn btn-primary center-block btn-style"><a style="color: white" href="{{route('equipmentOwner.create')}}">Dodaj vlasnika opreme</a></button>
                    @endif
            </div>
        </div>
    </div>
    </div>



@stop

@extends('layouts.footer')
