@extends ('layouts.admin')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {!! Form::open(['method'=>'POST', 'action'=>'EquipmentOwnersController@store', ]) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Ime vlasnika opreme') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    {!! Form::label('company', 'Naziv tvrtke') !!}
                    {!! Form::text('company', null, ['class'=>'form-control']) !!}

                    <div class="form-group">

                        {!! Form::label('comment', 'Komentar') !!}
                        {!! Form::text('comment', null, ['class'=>'form-control']) !!}

                    </div>

                    {!! Form::submit('Dodaj', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>


@endsection('footer')
