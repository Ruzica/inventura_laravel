@extends ('layouts.admin')

@section('content')

    <div class="col-md-6 col-md-offset-3">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">

                    {!! Form::model($equipmentOwner,['method'=>'PATCH', 'action'=>['EquipmentOwnersController@update', $equipmentOwner->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Ime vlasnika opreme') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    {!! Form::label('company', 'Naziv tvrtke') !!}
                    {!! Form::text('company', null, ['class'=>'form-control']) !!}

                    {!! Form::label('comment', 'Komentar') !!}
                    {!! Form::text('comment', null, ['class'=>'form-control form-group']) !!}

                    {!! Form::submit('Pohrani promjene', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                    {!! Form::open(['method'=>'DELETE', 'action'=>['EquipmentOwnersController@destroy', $equipmentOwner->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::submit('Ukloni vlasnika opreme', ['class'=>'btn btn-danger pull-right']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection('footer')