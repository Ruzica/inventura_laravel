@extends ('layouts.admin')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">

                    {!! Form::model($location,['method'=>'PATCH', 'action'=>['LocationsController@update', $location->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Ime lokacije') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    {!! Form::label('number_of_employees', 'Broj zaposlenika') !!}

                    {!! Form::text('number_of_employees', null, ['class'=>'form-control form-group']) !!}

                    {!! Form::submit('Pohrani promjene', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                    {!! Form::open(['method'=>'DELETE', 'action'=>['LocationsController@destroy', $location->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::submit('Ukloni lokaciju', ['class'=>'btn btn-danger pull-right']) !!}


                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection('footer')