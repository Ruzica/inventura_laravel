@extends ('layouts.admin')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['method'=>'POST', 'action'=>'LocationsController@store', ]) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Ime lokacije') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    {!! Form::label('number_of_employees', 'Broj zaposlenika') !!}
                    {!! Form::text('number_of_employees', null, ['class'=>'form-control form-group']) !!}

                    {!! Form::submit('Dodaj', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>

    </div>

@endsection('footer')

