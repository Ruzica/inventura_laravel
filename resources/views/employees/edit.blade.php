@extends ('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">

                    {!! Form::model($employee,['method'=>'PATCH', 'action'=>['EmployeesController@update', $employee->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Ime zaposlenika') !!}

                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    <div class="form-group">
                        {!! Form::Label('name', 'Lokacija') !!}
                        <select class="form-control" name="location_id">
                            @foreach($locations as $location)
                                <option value="{{$location->id}}"
                                @if ($employee->location->name ==$location->name )
                                    {{ 'selected' }}
                                        @endif
                                >{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    {!! Form::submit('Pohrani promjene', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                    {!! Form::open(['method'=>'DELETE', 'action'=>['EmployeesController@destroy', $employee->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::submit('Ukloni zaposlenika', ['class'=>'btn btn-danger pull-right']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>


@endsection('footer')