@extends ('layouts.admin')

@section('content')

    <div class="col-md-6 col-md-offset-3">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['method'=>'POST', 'action'=>'EmployeesController@store']) !!}
                    {{csrf_field()}}


                    {!! Form::label('name', 'Ime zaposlenika') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}


                    <div class="form-group">
                        {!! Form::Label('name', 'Lokacija') !!}
                        <select class="form-control" name="location_id">
                            @foreach($locations as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    {!! Form::submit('Dodaj', ['class'=>'btn btn-primary center-block btn-style']) !!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>




@endsection('footer')