@extends ('layouts.admin')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading">Zaposlenik</div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover dtable">
                        <thead align="center">
                        <tr>
                            <th>Ime zaposlenika</th>
                            <th>Lokacija zaposlenika</th>
                            @if(Auth::user()->role == "Administrator")
                                <th></th>
                                <th></th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($employees as $employee)

                            <tr>
                                <td>{{$employee->name}}</td>
                                <td>{{$employee->location->name ?? '*Lokacija uklonjena'}}</td>
                                @if(Auth::user()->role == "Administrator")
                                    <td><div class="text-center" ><a href="{{route('employees.edit', $employee->id)}}"><i class="glyphicon glyphicon-edit"></i></a></div></td>
                                    <td><div class="text-center" ><a href="{{route('employees.destroy', $employee->id)}}"><i class="glyphicon glyphicon-trash"></i></a></div></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(Auth::user()->role == "Administrator")
                        <button type="submit" class="btn btn-primary center-block btn-style"><a style="color: white" href="{{route('employees.create')}}">Dodaj zaposlenika</a></button>
                    @endif
                </div>
            </div>
        </div>
    </div>



@stop

@extends('layouts.footer')