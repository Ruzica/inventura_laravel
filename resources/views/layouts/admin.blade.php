<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inventura</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/Buttons-1.5.6/css/buttons.dataTables.min.css') }}"/>


    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->

    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-blue.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header style="background-color: #e7e7e7; " class="main-header">

        <!-- Logo -->
        <a style="background-color: #a58a8a"  href="{{ url('/') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>djsldjls</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Inventura</b></span>
        </a>
       </header>

        <!-- Header Navbar -->
        <nav style="background-color: #e7e7e7; margin-bottom: 0px;" class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">

            </div>
        </nav>

    <!-- Left side column. contains the logo and sidebar -->
    <aside style="background-color: #e7e7e7; border: #a58a8a"  class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                </div>
                <div  style="color: #a58a8a" class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a  style="color: #a58a8a" href="#"><i class="fa fa-circle text-success"></i> Online</a>

                </div>

            </div>

            <!-- search form (Optional) -->
            <form style="border-color: #ad9696" action="#" method="get" class="sidebar-form">

            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <!-- Optionally, you can add icons to the links -->
                <li><a style="color: #a58a8a;" href="{{ url('/equipment') }}"><i class="fa fa-desktop"></i> <span>Oprema</span></a></li>
                <li><a  style="color: #a58a8a; " href="{{ url('/locations') }}"><i class="fa fa-home"></i> <span>Lokacija</span></a></li>
                <li><a style="color: #a58a8a" href="{{ url('/employees') }}"><i class="fa fa-user"></i> <span>Zaposlenici</span></a></li>
                <li><a style="color: #a58a8a" href="{{ url('/equipmentOwner') }}"><i class="fa fa-id-badge"></i> <span>Vlasnici opreme</span></a></li>
                <li><a style="color: #a58a8a" href="{{ url('/users') }}"><i class="fa fa-female"></i> <span>Korisnici</span></a></li>
                <li><a style="color: #a58a8a" href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> <span>Odjava</span></a></li>

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div style="    margin-top: 80px;" class="col-sm-12">
        <!-- Content Header (Page header) -->

        <!-- Main content -->


         @yield ('content')

        </section>
        </div>
        <!-- /.content -->
    </div>    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2019 Designed by Ruzica </strong>
    </footer>

    <!-- Control Sidebar -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

<script src="{{ asset('assets/datatables/datatables.js') }}"></script>


<script type="text/javascript" src="{{ asset('assets/datatables/jQuery-3.3.1/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/Buttons-1.5.6/js/buttons.colVis.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/datatables/Buttons-1.5.6/js/buttons.print.min.js') }}"></script>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@yield('footer')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
<link rel="stylesheet" href="{{ asset('dist/css/skins/skin-blue.min.css') }}">
<style>
    .btn-style{
        background-color: #ad9696;
        border-color: #e7e7e7;
    }

</style>