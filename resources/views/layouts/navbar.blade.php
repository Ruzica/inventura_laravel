<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/user-icon.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a class="online-text" href="#"><i class="fa fa-circle text-success"></i>Dostupan</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li><a href="{{ url('/equipment') }}"><i class="fa fa-desktop"></i><span>Oprema</span></a></li>
            <li><a href="{{ url('/locations') }}"><i class="fa fa-home"></i><span>Lokacija</span></a></li>
            <li><a href="{{ url('/employees') }}"><i class="fa fa-user"></i><span>Zaposlenici</span></a></li>
            <li><a href="{{ url('/equipmentOwner') }}"><i class="fa fa-id-badge"></i><span>Vlasnici opreme</span></a></li>
            <li><a href="{{ url('/users') }}"><i class="fa fa-female"></i> <span>Korisnici</span></a></li>
            <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> <span>Odjava</span></a></li>
        </ul>
    </section>
</aside>