@section('footer')
    <script>
        $(document).ready( function () {
            $.noConflict();
            $('.dtable').DataTable({
                dom:'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],

                "language": {
                    "lengthMenu": "Prikaži _MENU_ zapisa po stranici",
                    "zeroRecords": "Nema zapisa!",
                    "info": "Prikazana stranica _PAGE_ od _PAGES_",
                    "infoEmpty": "Nema dostupnih zapisa!",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Traži:",
                    "paginate": {
                        "first": "Prvi",
                        "last": "Zadnji",
                        "next": "Sljedeća",
                        "previous": "Prethodna"
                    }}
            });
        } );

    </script>

@stop