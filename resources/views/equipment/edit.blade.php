@extends ('layouts.admin')

@section('content')

    <div class="col-md-6 col-md-offset-3">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">

                    {!! Form::model($equipment,['method'=>'PATCH', 'action'=>['EquipmentsController@update', $equipment->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Naziv opreme') !!}

                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    <div class="form-group">
                        {!! Form::Label('name', 'Lokacija') !!}
                        <select class="form-control" name="location_id">
                            @foreach($locations as $location)
                                <option value="{{$location->id}}"
                                @if ($equipment->location->name ==$location->name )
                                    {{ 'selected' }}
                                        @endif
                                >{{$location->name ?? '*Lokacija uklonjena'}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {!! Form::Label('name', 'Vlasnik opreme') !!}
                        <select class="form-control" name="equipment_owner_id">
                            @foreach($equipmentOwners as $equipmentOwner)
                                <option value="{{$equipmentOwner->id}}"
                                @if ($equipment->equipmentOwner->name ==$equipmentOwner->name )
                                    {{ 'selected' }}
                                        @endif
                                >{{$equipmentOwner->name ?? '*Vlasnik opreme uklonjen'}}</option>
                            @endforeach
                        </select>
                    </div>

                    {!! Form::label('pn_number', 'PN broj') !!}
                    {!! Form::text('pn_number', null, ['class'=>'form-control']) !!}

                    {!! Form::label('serial_number', 'Serijski broj') !!}
                    {!! Form::text('serial_number', null, ['class'=>'form-control']) !!}

                    {!! Form::label('model', 'Model opreme') !!}
                    {!! Form::text('model', null, ['class'=>'form-control']) !!}

                    {!! Form::label('comment', 'Komentar') !!}
                    {!! Form::text('comment', null, ['class'=>'form-control form-group']) !!}

                    {!! Form::submit('Pohrani promjene', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                    {!! Form::open(['method'=>'DELETE', 'action'=>['EquipmentsController@destroy', $equipment->id]]) !!}
                    {{csrf_field()}}

                    {!! Form::submit('Ukloni opremu', ['class'=>'btn btn-danger pull-right']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection('footer')
<style>
    .col-centered{
        float: none;
        margin: 0 auto;
    }

</style>
