@extends ('layouts.admin')

@section('content')

    <div class="col-md-8 col-md-offset-2">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'EquipmentsController@store']) !!}
                    {{csrf_field()}}

                    {!! Form::label('name', 'Naziv opreme') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    <div class="form-group">
                        {!! Form::Label('name', 'Lokacija') !!}
                        <select class="form-control" name="location_id">
                            @foreach($locations as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        {!! Form::Label('name', 'Vlasnik opreme') !!}
                        <select class="form-control" name="equipment_owner_id">
                            @foreach($equipmentOwners as $equipmentOwner)
                                <option value="{{$equipmentOwner->id}}">{{$equipmentOwner->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    {!! Form::label('pn_number', 'PN broj') !!}
                    {!! Form::text('pn_number', null, ['class'=>'form-control']) !!}

                    {!! Form::label('serial_number', 'Serijski broj') !!}
                    {!! Form::text('serial_number', null, ['class'=>'form-control']) !!}

                    {!! Form::label('model', 'Model opreme') !!}
                    {!! Form::text('model', null, ['class'=>'form-control']) !!}

                    <div class="form-group">

                        {!! Form::label('comment', 'Komentar') !!}
                        {!! Form::text('comment', null, ['class'=>'form-control']) !!}

                    </div>

                    {!! Form::submit('Dodaj', ['class'=>'btn btn-primary center-block btn-style']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>



@endsection('footer')





