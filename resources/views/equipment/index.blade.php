@extends ('layouts.admin')

@section('content')

    <div class="col-md-10 col-md-offset-1">
        <div class="box-body">
            <div class="panel panel-default">
                <div class="panel-heading">Oprema</div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover dtable">
                        <thead align="center">
                        <tr>
                            <th>Naziv opreme</th>
                            <th>Lokacija opreme</th>
                            <th>Vlasnik opreme</th>
                            <th>PN broj</th>
                            <th>Serijski broj</th>
                            <th>Model</th>
                            <th>Komentar</th>
                            @if(Auth::user()->role == "Administrator")
                                <th></th>
                                <th></th> <th></th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($equipments as $equipment)
                            <tr>
                                <td>{{$equipment->name}}</td>
                                <td>{{$equipment->location->name ?? '*Lokacija uklonjena'}}</td>
                                <td>{{$equipment->equipmentOwner->name ?? '*Vlasnik opreme uklonjen'}}</td>
                                <td>{{$equipment->pn_number}}</td>
                                <td>{{$equipment->serial_number}}</td>
                                <td>{{$equipment->model}}</td>
                                <td>{{$equipment->comment}}</td>
                                @if(Auth::user()->role == "Administrator")
                                     <td><div class="text-center"><a href="{{route('equipment.show', $equipment->id)}}"><i class="glyphicon glyphicon-eye-open"></i></a></div></td>
                                     <td><div class="text-center" ><a href="{{route('equipment.edit', $equipment->id)}}"><i class="glyphicon glyphicon-edit"></i></a></div></td>
                                    <td><div class="text-center" ><a href="{{route('equipment.destroy', $equipment->id)}}"><i class="glyphicon glyphicon-trash"></i></a></div></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(Auth::user()->role == "Administrator")
                        <button type="submit" class="btn btn-primary center-block btn-style"><a style="color: white"
                                                                                                href="{{route('equipment.create')}}">Dodaj opremu</a></button>
                    @endif
                </div>

            </div>

        </div>
    </div>


@stop

@extends('layouts.footer')